class ChangeDateTimeToDate < ActiveRecord::Migration
  def up
    change_column :cats, :birth_date, :date
  end

  def down
  end

end
