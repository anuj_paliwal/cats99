

module SessionsHelper

  def current_user
    return nil if session[:session_token].nil?
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def current_user=(user)

    @current_user = user
    session[:session_token] = user.session_token

  end


  def login_user!
    # @user = User.find_by_credentials(
#       params[:user][:user_name],
#       params[:user][:password]
#     )
#
#     if @user.nil?
#       flash.now[:notices] = "Invalid login"
#     else
#       new_token = @user.reset_session_token!
#       session[:session_token] = new_token
#       @current_user = @user
#     end
  end

end
