class CatRentalRequestsController < ApplicationController

  before_filter :ensure_ownerness, only: [:approve, :deny]

  def new
    @cats = Cat.all
    render :new
  end

  def create
    CatRentalRequest.create!(params[:cat_rental_request])

    redirect_to cat_rental_requests_url
  end

  def index
    @cat_rental_requests = CatRentalRequest.all
  end

  def approve
    @cat_rental_request = CatRentalRequest.find(params[:id])
    @cat_rental_request.approve!
    flash[:notices] = "Your request was approved"
    redirect_to cats_url
  end

  def deny
    @cat_rental_request = CatRentalRequest.find(params[:id])
    @cat_rental_request.deny!
    flash[:notices] = "Your request was denied"
    redirect_to cats_url
  end

  private
  def ensure_ownerness
    @cat_rental_request = CatRentalRequest.find(params[:id])
    unless @cat_rental_request.cat.owner.id == current_user.id
      redirect_to cats_url
    end
  end

end
