class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])



    if @user.save
      flash[:notice] = "User Saved"
      self.current_user = @user
      redirect_to user_url(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end


  end

  def show
    @user = User.find(params[:id])
  end
end
