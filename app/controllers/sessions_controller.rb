class SessionsController < ApplicationController

  # def new
  # end

  def create
    @user = User.find_by_credentials(
      params[:user][:user_name],
      params[:user][:password]
    )

    if @user.nil?
      flash.now[:notices] = "Invalid login"
    else
      session[:session_token] = @user.session_token
      @current_user = @user
    end
    redirect_to user_url(@user)
  end

  def destroy
    @user = User.find_by_session_token(session[:session_token])
    @user.reset_session_token!
    session[:session_token] = nil
    redirect_to cats_url
  end



end
