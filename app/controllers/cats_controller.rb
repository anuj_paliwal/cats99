class CatsController < ApplicationController

  before_filter :ensure_ownerness, only: [:edit, :update]

  def index
    @cats = Cat.all
  end

  def show
    @cat = Cat.find_by_id(params[:id])
    @rental_requests = @cat.rental_requests
  end

  def new
    @cat = Cat.new

    render :new
  end

  def create
    # Add validation
    c = Cat.create!(params[:cat])
    c.user_id = self.current_user.id
    c.save!

    redirect_to cat_url(c)
  end

  def edit
    @cat = Cat.find(params[:id])
    render :edit
  end

  def update
    c = Cat.find(params[:id])
    c.update_attributes(params[:cat])

    redirect_to cat_url(c)
  end

  private
  def ensure_ownerness
    @cat = Cat.find_by_id(params[:id])
    unless @cat.user_id == current_user.id
      redirect_to cats_url
    end
  end

end

