require 'debugger'

class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :start_date, :end_date, :status
  before_save :default_values
  validates_inclusion_of :status, in: %w(PENDING APPROVED DENIED)
  validate :overlapping_approved_requests

  def default_values
    self.status ||= 'PENDING'
  end

  belongs_to(
    :cat,
    :class_name => 'Cat',
    :foreign_key => :cat_id,
    :primary_key => :id
  )

  def approve!
    self.status = "APPROVED"
    ActiveRecord::Base.transaction do
      self.status = "APPROVED"
      self.save!
      overlapping_pending_requests.each do |request|
        request.status = "DENIED"
        request.save!
      end
    end
  end

  def deny!
    self.status = "DENIED"
    self.save!
  end

  def range_overlap?(request1, request2)
    x, y = request1, request2
    (x.start_date - y.end_date) * (y.start_date - x.end_date) >= 0
  end

  def overlapping_requests
    cat = Cat.find(self.cat_id)

    overlapping_requests = []
    cat.rental_requests.each do |request|
      overlapping_requests << request if range_overlap?(self, request)
    end

    overlapping_requests
  end

  def overlapping_approved_requests
    overlapping_approved = []
    #debugger
    overlapping_requests.each do |request|
      overlapping_approved << request if request.status == "APPROVED"
    end

    if self.status != "DENIED" && overlapping_approved.any?
      errors.add(:start_date, "Date range conflict")
    end

  end

  def overlapping_pending_requests
    overlapping_pending = []
    overlapping_requests.each do |request|
      overlapping_pending << request if request.status == "PENDING"
    end

    overlapping_pending
  end



end















