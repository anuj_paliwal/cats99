class Cat < ActiveRecord::Base
  attr_accessible :age, :birth_date, :color, :name, :sex, :user_id
  validates_inclusion_of :color, in: %w(black white gray calico)
  validates_inclusion_of :sex, in: %w(M F)

  has_many(
    :rental_requests,
    :class_name => 'CatRentalRequest',
    :foreign_key => :cat_id,
    :primary_key => :id,
    dependent: :destroy,
    :order => :start_date
  )

  belongs_to(
    :owner,
    :class_name => "User",
    :foreign_key => :user_id,
    :primary_key => :id
  )

end
